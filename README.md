# Step framework

# Rules for step classes
* Every Step must implement App\Step interface.
* All exceptions are implemented by Step classes.
* If you want to implement instruction, which uses 2 parameters (A & B, for example), simple instantiate from BaseAB class. All needed logic is incapsulated inside App\BaseAB.

#Example for new Step class
```
// src/Step/Sub.php
namespae App\Step;

use App\Step;

class Sub extends BaseAB
{
    public function runInternal(Context $c)
    {
        $value = $c->getVal($this->valA) - $c->getVal($this->valB);
        $c->setVal($this->valOut, $value);
    }
}
```
In this case $c - is a context for storing data between all steps

Also you need to add declarations to App\Process function stepFactory(...)
```
  case 'sub':
    $step = Sub::getInstance($stepItem);
    break;
```
If you do not want to use BaseAB class as a base class, you should simply make new instance of your class
```
  case 'sub':
    $step = new Sub( <params> );
    break;
```

# Ordinary usage example

```use App\App;

$app = new App(
__DIR__ . '/config',
__DIR__ . '/data');

$app->process();
$app->getData()->writeFile(__DIR__ . '/data_out');
```

There are 2 params for App\App constructor. First param is config file. Second params is initial input data

If you want to write a file (json format). Write following code.
```
$app->getData->writeFile(<xxx>)
```
where <xxx> is filename.

#Config format
In this case we are trying to implement (a + b) * c
```
add,res2,a,b
mul,result,res2,c
```
Result will be stored in result pair value

* Config file is a plain txt file
* All steps are written one by one.
* You have to deasfribe all fields, which you are trying to use in step class. In this case App\Step\Add and App\Step\Mul we also define result field. In this field result will be stored.
* The name of each step is the name of step class


#Data file
```
a:2
b:3
c:5

```

You simply define a text file with fields and values.

# Run process

You have to be installed docker and docker-compose
```
make run
```
