<?php

namespace App\Config;

class Config implements \Iterator
{
    private ?ConfigLine $line;
    /**
     * @var false|resource
     */
    private $fd;
    private bool $end = false;

    function __construct(string $fn)
    {
        $this->fd = fopen($fn, 'r');
        $this->next();
    }

    public function current()
    {
        return $this->line;
    }

    public function next()
    {
        $data = fgets($this->fd);
        if ($data !== false) {
            $this->line = ConfigLine::parseString($data);
        } else {
            $this->end = true;
            fclose($this->fd);
        }
    }

    public function key()
    {
        return false;
    }

    public function valid()
    {
        return !$this->end;
    }

    public function rewind()
    {
        return false;
    }
}
