<?php

namespace App\Config;

class ConfigLine
{
    private $name;
    private $result;
    private array $operands;

    function __construct($name, $result, ...$operands)
    {
        $this->name = $name;
        $this->result = $result;
        $this->operands = $operands;
    }

    function getName()
    {
        return $this->name ?? false;
    }

    function getResult()
    {
        return $this->result ?? false;
    }

    function getA()
    {
        return $this->operands[0] ?? false;
    }

    function getB()
    {
        return $this->operands[1] ?? false;
    }

    public static function parseString(string $str): ?ConfigLine
    {
        $data = array_map(fn($item) => rtrim($item), explode(',', $str));
        if (count($data) > 2) {
            return new ConfigLine(...$data);
        } else {
            return null;
        }
    }
}
