<?php

namespace App;

class Context
{
    private array $data;
    private \Redis $redis;

    function __construct()
    {
        $this->redis = new \Redis();
        $this->redis->connect('redis', 6379);
        $this->redis->setOption(\Redis::OPT_SCAN, \Redis::SCAN_RETRY);
    }

    function fromFile(string $fn): Context
    {
        if (file_exists($fn)) {
            $context = new Context();
            $fd = fopen($fn, 'r');
            while(true) {
                $data = fgets($fd);
                if ($data == false) break;
                list($name, $val) = array_map(fn($i) => rtrim($i), explode(':', $data));
                $context->setVal($name, $val);
            }
            return $context;
        } else {
            throw new \Exception('No data file found:' . $fn);
        }
    }

    public function writeFile(string $fn)
    {
        $iterator = null;
        $fd_out = fopen($fn, 'w+');
        while(false !== ($keys = $this->redis->scan($iterator, '*'))) {
             foreach($keys as $key) {
                fputs($fd_out, $key . ':'. $this->redis->get($key) . "\n");
                $this->redis->del($key);
             }
        }
        fclose($fd_out);
    }

    public function setVal($name, $val)
    {
        $this->redis->set($name, $val);
    }

    public function getVal($name)
    {
        return $this->redis->get($name);
    }

    public function __toString(): string
    {
        return print_r($this->redis->keys('*'), true);
    }
}
