<?php

namespace App;

use App\Config\Config;
use App\Config\ConfigLine;
use App\Step\Add;
use App\Step\Mul;

class Process
{
    /**
     * @throws \Exception
     */
    function execute(Config $config, Context $context)
    {
        foreach ($config as $stepConfig) {
            $step = $this->stepFactory($stepConfig);
            $step->run($context);
        }
    }

    /**
     * @throws \Exception
     */
    function stepFactory(ConfigLine $cl): Step
    {
        switch($cl->getName()) {
            case 'add':
                $step = Add::getInstance($cl);
                break;
            case 'mul':
                $step = Mul::getInstance($cl);
                break;
            default:
                throw new \Exception('No proper step: ' . $cl->getName());
        }
        return $step;
    }
}
