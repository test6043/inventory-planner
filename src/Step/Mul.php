<?php

namespace App\Step;

use App\Context;

class Mul extends BaseAB
{
    public function runInternal(Context $c)
    {
        $value = $c->getVal($this->valA) * $c->getVal($this->valB);
        $c->setVal($this->valOut, $value);
    }
}
