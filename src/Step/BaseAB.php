<?php

namespace App\Step;

use App\Config\ConfigLine;
use App\Context;
use App\Step;

abstract class BaseAB implements Step
{
    protected string $valA = 'a';
    protected string $valB = 'b';
    protected string $valOut = 'result';

    function __construct(string $valA, string $valB, string $result)
    {
        $this->valA = $valA;
        $this->valB = $valB;
        $this->valOut = $result;
    }

    /**
     * @throws \Exception
     */
    public static function getInstance(ConfigLine $config): Step
    {
        if (!$config->getA() || !$config->getB() || !$config->getResult()) {
            throw new \Exception('Insufficient fields in config step ' . static::class);
        }
        return new static($config->getA(), $config->getB(), $config->getResult());
    }

    const NO_FIELD = 'No field in data: ';
    public function run(Context $data)
    {
        if (!$data->getVal($this->valA)) {
            throw new \Exception(self::NO_FIELD . $this->valA);
        }

        if (!$data->getVal($this->valB)) {
            throw new \Exception(self::NO_FIELD . $this->valB);
        }

        $this->runInternal($data);
    }

    abstract function runInternal(Context $data);
}
