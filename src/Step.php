<?php

namespace App;


use App\Config\ConfigLine;

interface Step
{
    public static function getInstance(ConfigLine $config): Step;
    public function run(Context $data);
}
