<?php

namespace App;

use App\Config\Config;

class App
{
    private $context;

    public function __construct(string $configFile, string $dataFile)
    {
        $this->context = new Context();
        $this->context->fromFile($dataFile);
        $this->config = new Config($configFile);
    }

    public function process()
    {
        $process = new Process();
        $process->execute($this->config, $this->context);
    }

    public function getData(): Context
    {
        return $this->context;
    }
}
