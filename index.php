<?php
require('vendor/autoload.php');

use App\App;

$app = new App(
    __DIR__ . '/config',
    __DIR__ . '/data');

$app->process();
$app->getData()->writeFile(__DIR__ . '/data_out');
